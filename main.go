package main

import (
	"log"
	"os"
	"os/signal"
	"strings"
	"syscall"

	"gitlab.com/ParkerMc/ParkerBotGo/discord"
	"gitlab.com/ParkerMc/ParkerBotGo/handlers"
	"gitlab.com/ParkerMc/ParkerBotGo/model"
	"gitlab.com/ParkerMc/ParkerBotGo/modloader"
	"gitlab.com/ParkerMc/ParkerBotGo/mongo"
)

func main() {
	var client model.Client
	moduleLoader := &modloader.ModuleLoader{}

	config, err := model.LoadConfig("configs/main.json")
	if err != nil {
		log.Fatal(err)
	}

	if strings.ToLower(config.Driver) == "discord" {
		client = &discord.Client{}
	} else {
		log.Fatal("Config doesn't have a vaild driver set")
	}

	database := mongo.Create()
	database.Connect(&config)

	var handlersMgr model.Handlers = handlers.Create(database)
	err = client.Init(handlersMgr)
	if err != nil {
		log.Print(err)
	}

	moduleLoader.Load(client, handlersMgr)
	err = client.Start()
	if err != nil {
		log.Print(err)
	}

	log.Print("Bot is now running. Press CTRL-C to exit.")
	stop := make(chan os.Signal, 1)
	signal.Notify(stop, syscall.SIGINT, syscall.SIGTERM, os.Interrupt, os.Kill)
	<-stop
	log.Print("Stoping bot")

	err = client.Stop()
	if err != nil {
		log.Print(err)
	}
	log.Print("Disconnected from chat service.")
	database.Disconnect()
	log.Print("Disconnected from database.")

}
