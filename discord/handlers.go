package discord

import (
	"log"

	"github.com/bwmarrin/discordgo"
	"gitlab.com/ParkerMc/ParkerBotGo/model"
)

func (client *Client) channelEvent(session *discordgo.Session, event *discordgo.ChannelCreate) {
	channel := client.discordChannelConverter(event.Channel)
	client.handlers.TriggerChannelHandler(&channel)
}

func (client *Client) channelDeleteEvent(session *discordgo.Session, event *discordgo.ChannelDelete) {
	channel := client.discordChannelConverter(event.Channel)
	client.handlers.TriggerChannelDeleteHandler(channel.ID, client)
}

func (client *Client) channelUpdateEvent(session *discordgo.Session, event *discordgo.ChannelUpdate) {
	channel := client.discordChannelConverter(event.Channel)
	client.handlers.TriggerChannelUpdateHandler(&channel)
}

func (client *Client) connectEvent(session *discordgo.Session, event *discordgo.Connect) {
	log.Print("Connected to discord.")
	client.handlers.TriggerConnectHandler(client)
}

func (client *Client) disconnectEvent(session *discordgo.Session, event *discordgo.Disconnect) {
	log.Print("Disconnected from discord.")
}

func (client *Client) messageCreateEvent(session *discordgo.Session, event *discordgo.MessageCreate) {
	message, err := client.discordMessageConverter(event.Message)
	if err != nil {
		log.Print("Error in message create event, " + err.Error())
	}
	client.handlers.TriggerMessageHandler(&message)
}

func (client *Client) messageDeleteEvent(session *discordgo.Session, event *discordgo.MessageDelete) {
	var clientPtr model.Client = client
	client.handlers.TriggerMessageDeleteHandler(event.ID, clientPtr)
}

func (client *Client) messageReaction(session *discordgo.Session, event *discordgo.MessageReactionAdd) {
	reaction := client.discordReactionConverter(event.MessageReaction)
	client.handlers.TriggerMessageReactionHandler(&reaction)
}

func (client *Client) messageDeleteReaction(session *discordgo.Session, event *discordgo.MessageReactionRemove) {
	reaction := client.discordReactionConverter(event.MessageReaction)
	client.handlers.TriggerMessageReactionDeleteHandler(&reaction)
}

func (client *Client) messageUpdateEvent(session *discordgo.Session, event *discordgo.MessageUpdate) {
	message, err := client.discordMessageConverter(event.Message)
	if err != nil {
		log.Print("Error in message update event, " + err.Error())
	}
	client.handlers.TriggerMessageUpdateHandler(&message)
}

func (client *Client) typeingStartEvent(session *discordgo.Session, event *discordgo.TypingStart) {
	typeingStart := client.discordTypingStartConverter(event)
	client.handlers.TriggerTypingStart(&typeingStart)
}
