package discord

import (
	"gitlab.com/ParkerMc/ParkerBotGo/model"
)

// GetServers returns the servers that the bot is in
func (client *Client) GetServers() ([]model.Server, error) {
	serverList := make([]model.Server, 0)
	lastCount := 100
	lastID := ""
	for lastCount == 100 {
		serverListPart, err := client.session.UserGuilds(100, "", lastID)
		if err != nil {
			return nil, err
		}
		for _, guild := range serverListPart {
			fullGuild, err := client.session.Guild(guild.ID)
			if err != nil {
				return nil, err
			}
			server, err := client.discordGuildConverter(fullGuild)
			if err != nil {
				return nil, err
			}
			serverList = append(serverList, server)
			lastID = guild.ID
		}
		lastCount = len(serverListPart)
	}
	return serverList, nil
}
