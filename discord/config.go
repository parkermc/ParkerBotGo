package discord

import (
	"encoding/json"
	"errors"
	"io/ioutil"
	"os"
	"path/filepath"
)

// Config object for the config file
type Config struct {
	Token string `json:"token"` // The driver to be used
}

// GetConfigDefault returns the default config
func GetConfigDefault() Config {
	return Config{
		Token: "",
	}
}

// ToJSON converts the object to a json string
func (config *Config) ToJSON() ([]byte, error) {
	return json.MarshalIndent(config, "", " ")
}

// LoadConfig Loads the config
func LoadConfig(filename string) (Config, error) {
	config := GetConfigDefault()
	if _, err := os.Stat(filename); err == nil { // If the file exists
		configFile, err := os.Open(filename) // Read it
		if err != nil {
			return config, errors.New("Error while loading config: " + err.Error())
		}
		jsonParser := json.NewDecoder(configFile) // Decode the json
		jsonParser.Decode(&config)
		configFile.Close()
	}
	if _, err := os.Stat(filepath.Dir(filename)); os.IsNotExist(err) { // If the folder doesn't exist create it
		err = os.MkdirAll(filepath.Dir(filename), 0655)
		if err != nil {
			return config, errors.New("Error while creating folder for config config: " + err.Error())
		}
	}
	configJSON, err := config.ToJSON() // Get a string from the config
	if err != nil {
		return config, errors.New("Error while converting the config object to json: " + err.Error())
	}
	err = ioutil.WriteFile(filename, configJSON, 0655) // Write it to the file
	if err != nil {
		return config, errors.New("Error while saving config: " + err.Error())
	}
	return config, nil
}
