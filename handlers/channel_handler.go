package handlers

import (
	"log"
	"time"

	"gitlab.com/ParkerMc/ParkerBotGo/model"
)

// RegisterChannelHandler registers a channel handler to the list
func (handlers *Handlers) RegisterChannelHandler(channelHandler func(*model.Channel)) {
	handlers.channelHandlers = append(handlers.channelHandlers, channelHandler)
}

// TriggerChannelHandler Triggers the channel handlers
func (handlers *Handlers) TriggerChannelHandler(channel *model.Channel) {
	err := handlers.database.AddChannel(channel)
	if err != nil {
		log.Print("Error adding channel to database: " + err.Error())
	}
	for _, channelHandler := range handlers.channelHandlers {
		channelHandler(channel)
	}
}

// RegisterChannelDeleteHandler registers a channel delete handler to the list
func (handlers *Handlers) RegisterChannelDeleteHandler(channelDeleteHandler func(*model.Channel)) {
	handlers.channelDeleteHandlers = append(handlers.channelDeleteHandlers, channelDeleteHandler)
}

// TriggerChannelDeleteHandler Triggers the channel delete handlers
func (handlers *Handlers) TriggerChannelDeleteHandler(channelID string, client model.Client) {

	channel, err := handlers.database.GetChannel(channelID)
	channel.Client = &client
	if err != nil {
		log.Print("Error getting deleted channel: " + err.Error())
		return
	}
	for _, channelDeleteHandler := range handlers.channelDeleteHandlers {
		channelDeleteHandler(channel)
	}
}

// RegisterChannelUpdateHandler registers a channel update handler to the list
func (handlers *Handlers) RegisterChannelUpdateHandler(channelUpdateHandler func(*model.Channel)) {
	handlers.channelUpdateHandlers = append(handlers.channelUpdateHandlers, channelUpdateHandler)
}

// TriggerChannelUpdateHandler Triggers the channel update handlers
func (handlers *Handlers) TriggerChannelUpdateHandler(newChannel *model.Channel) {
	channel, err := handlers.database.GetChannel(newChannel.ID)
	if err != nil {
		log.Print("Error getting edited channel: " + err.Error())
		channel = newChannel
	} else {
		channel.PreEdits = append(channel.PreEdits, model.ChannelEdit{
			Name:      channel.Name,
			Timestamp: time.Now(),
			Topic:     channel.Topic,
		})
		channel.Name = newChannel.Name
		channel.Topic = newChannel.Topic
		channel.Client = newChannel.Client
	}
	handlers.database.UpdateChannel(channel)
	for _, channelUpdateHandler := range handlers.channelUpdateHandlers {
		channelUpdateHandler(channel)
	}
}
