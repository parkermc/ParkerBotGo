package handlers

import (
	"log"

	"gitlab.com/ParkerMc/ParkerBotGo/model"
)

// RegisterMessageHandler registers a message handler to the list
func (handlers *Handlers) RegisterMessageHandler(msgHandler func(*model.Message)) {
	handlers.messageHandlers = append(handlers.messageHandlers, msgHandler)
}

// TriggerMessageHandler Triggers the message handlers
func (handlers *Handlers) TriggerMessageHandler(message *model.Message) {
	err := handlers.database.AddMessage(message)
	if err != nil {
		log.Print("Error adding message to database: " + err.Error())
	}
	for _, msgHandler := range handlers.messageHandlers {
		msgHandler(message)
	}
}

// RegisterMessageUpdateHandler registers a message update handler to the list
func (handlers *Handlers) RegisterMessageUpdateHandler(msgUpdateHandler func(*model.Message)) {
	handlers.messageUpdateHandlers = append(handlers.messageUpdateHandlers, msgUpdateHandler)
}

// TriggerMessageUpdateHandler Triggers the message update handlers
func (handlers *Handlers) TriggerMessageUpdateHandler(newMessage *model.Message) {
	message, err := handlers.database.GetMessage(newMessage.ID)
	if err != nil {
		log.Print("Error getting edited message: " + err.Error())
		message = newMessage
	} else {
		message.PreEdits = append(message.PreEdits, model.MessageEdit{
			Msg:       message.Msg,
			Timestamp: newMessage.EditedTimestamp,
		})
		message.EditedTimestamp = newMessage.EditedTimestamp
		message.Msg = newMessage.Msg
		message.Client = newMessage.Client
	}
	handlers.database.UpdateMessage(message)
	for _, msgUpdateHandler := range handlers.messageUpdateHandlers {
		msgUpdateHandler(message)
	}
}

// RegisterMessageDeleteHandler registers a message delete handler to the list
func (handlers *Handlers) RegisterMessageDeleteHandler(msgDeleteHandler func(*model.Message)) {
	handlers.messageDeleteHandlers = append(handlers.messageDeleteHandlers, msgDeleteHandler)
}

// TriggerMessageDeleteHandler Triggers the message delete handlers
func (handlers *Handlers) TriggerMessageDeleteHandler(messageID string, client model.Client) {
	message, err := handlers.database.GetMessage(messageID)
	message.Client = &client
	if err != nil {
		log.Print("Error getting deleted message: " + err.Error())
		return
	}
	for _, msgDeleteHandler := range handlers.messageDeleteHandlers {
		msgDeleteHandler(message)
	}
}

// RegisterMessageReactionHandler registers a message reaction handler to the list
func (handlers *Handlers) RegisterMessageReactionHandler(msgReactionHandler func(*model.MessageReaction)) {
	handlers.messageReactionHandlers = append(handlers.messageReactionHandlers, msgReactionHandler)
}

// TriggerMessageReactionHandler Triggers the message reaction handlers
func (handlers *Handlers) TriggerMessageReactionHandler(messageReaction *model.MessageReaction) {
	for _, msgReactionHandler := range handlers.messageReactionHandlers {
		msgReactionHandler(messageReaction)
	}
}

// RegisterMessageReactionDeleteHandler registers a message reaction delete handler to the list
func (handlers *Handlers) RegisterMessageReactionDeleteHandler(msgReactionDeleteHandler func(*model.MessageReaction)) {
	handlers.messageReactionDeleteHandlers = append(handlers.messageReactionDeleteHandlers, msgReactionDeleteHandler)
}

// TriggerMessageReactionDeleteHandler Triggers the message reaction delete handlers
func (handlers *Handlers) TriggerMessageReactionDeleteHandler(messageReaction *model.MessageReaction) {
	for _, msgReactionDeleteHandler := range handlers.messageReactionDeleteHandlers {
		msgReactionDeleteHandler(messageReaction)
	}
}
