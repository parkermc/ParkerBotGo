package utils

import (
	"strings"
)

var messageLegnthLimit = 1990 // Set a reasonable limit for the max legnth

// MessageSplitter Splits a message so it isn't too long used 0 for just under 2,000
func MessageSplitter(message string, messageLimit int) []string {
	if messageLimit <= 0 {
		messageLimit = messageLegnthLimit
	}
	splitMessage := make([]string, 0)
	messagePart := ""
	splitLines := strings.Split(message, "\n")
	for _, line := range splitLines {
		if len(line) > messageLimit {
			splitSpaces := strings.Split(line, " ")
			for _, part := range splitSpaces {
				if len(part) > messageLimit {
					partRunes := []rune(part)
					splitMessage = append(splitMessage, messagePart)
					messagePart = ""
					for i := 0; i*messageLimit < len(partRunes); i++ {
						if (i+1)*messageLimit >= len(partRunes) {
							splitMessage = append(splitMessage, string(partRunes[i*messageLimit:len(partRunes)-1]))

						} else {
							splitMessage = append(splitMessage, string(partRunes[i*messageLimit:(i+1)*messageLimit]))
						}
					}
				} else if len(part)+len(messagePart) > messageLimit {
					splitMessage = append(splitMessage, messagePart)
					messagePart = part
				} else {
					messagePart += part
				}
			}
		} else if len(line)+len(messagePart) > messageLimit {
			splitMessage = append(splitMessage, messagePart)
			messagePart = line
		} else {
			messagePart += line
		}
	}
	splitMessage = append(splitMessage, messagePart)
	return splitMessage
}
