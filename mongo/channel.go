package mongo

import (
	"errors"

	"gitlab.com/ParkerMc/ParkerBotGo/model"

	"gopkg.in/mgo.v2/bson"
)

// TODO move most of this stuff under servers

const channelsCollectionName string = "channels"

// AddChannel add a channel to the database
func (mongo *Mongo) AddChannel(channel *model.Channel) error {
	return mongo.database.C(channelsCollectionName).Insert(channel)
}

// GetChannel gets a channel from the database by ID
func (mongo *Mongo) GetChannel(channelID string) (*model.Channel, error) {
	count, err := mongo.database.C(channelsCollectionName).Find(&bson.M{"channelID": channelID}).Count()
	if err != nil {
		return nil, err
	}
	if count == 0 {
		return nil, errors.New("Channel not found")
	}
	channel := model.Channel{}
	err = mongo.database.C(channelsCollectionName).Find(&bson.M{"channelID": channelID}).One(&channel)
	if err != nil {
		return nil, err
	}
	return &channel, nil
}

// UpdateChannel update a channel in the database by ID
func (mongo *Mongo) UpdateChannel(channel *model.Channel) error {
	count, err := mongo.database.C(channelsCollectionName).Find(&bson.M{"_id": channel.DatabaseID}).Count()
	if err != nil {
		return err
	}
	if count == 0 {
		return errors.New("Channel not found")
	}

	err = mongo.database.C(channelsCollectionName).Update(&bson.M{"_id": channel.DatabaseID}, channel)
	if err != nil {
		return err
	}
	return nil
}

// GetChannelIDs get all the ids of channels in the databse
func (mongo *Mongo) GetChannelIDs() ([]string, error) {
	channels := make([]model.Channel, 0)
	err := mongo.database.C(channelsCollectionName).Find(&bson.M{}).All(&channels)
	if err != nil {
		return nil, errors.New("Error getting channels: " + err.Error())
	}

	ids := make([]string, 0)
	for _, channel := range channels {
		ids = append(ids, channel.ID)
	}
	return ids, nil
}
