package model

// MessageAttachment used to keep the attachments to the messges
type MessageAttachment struct {
	ID   string `bson:"id"`
	Name string `bson:"name"`
	URL  string `bson:"url"`
}
