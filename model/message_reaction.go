package model

// MessageReaction holds the data for a reaction on a message
type MessageReaction struct {
	ChannelID string
	Client    *Client
	Emoji     Emoji
	MessageID string
	UserID    string
}
