package model

import (
	"time"

	"gopkg.in/mgo.v2/bson"
)

// User used for the user type
type User struct {
	AvatarURL    string        `bson:"avatarURL"`
	Bot          bool          `bson:"bot"`
	DatabaseID   bson.ObjectId `bson:"_id,omitempty"`
	FullUsername string        `bson:"fUsername"`
	ID           string        `bson:"userId"`
	Timestamp    time.Time     `bson:"ts"`
	Username     string        `bson:"username"`
}
