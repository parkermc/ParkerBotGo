package model

// ServerMember is the object that hold the user in a server
type ServerMember struct { // TODO add roles
	Nickname string
	Joined   string
	User     User
}
