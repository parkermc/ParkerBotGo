package model

// Server stores the servers
type Server struct {
	ID          string
	Name        string
	OwnerID     string
	Icon        string
	MemberCount int
	Emojis      []Emoji
	Members     []ServerMember
	Channels    []Channel
}
