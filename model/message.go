package model

import (
	"time"

	"gopkg.in/mgo.v2/bson"
)

// Message How every message is stored
type Message struct {
	Attachment      MessageAttachment `bson:"attachment"`
	Author          User              `bson:"author"`
	ChannelID       string            `bson:"channelID"`
	Client          *Client           `bson:"-"`
	DatabaseID      bson.ObjectId     `bson:"_id,omitempty"`
	ID              string            `bson:"msgId"`
	Timestamp       time.Time         `bson:"ts"`
	EditedTimestamp time.Time         `bson:"ets"`
	Msg             string            `bson:"msg"`
	PreEdits        []MessageEdit     `bson:"pre_edits"`
}

// MessageEdit used to keep track of the edits
type MessageEdit struct {
	Msg       string    `bson:"msg"`
	Timestamp time.Time `bson:"ts"`
}
