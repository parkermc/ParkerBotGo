package helloworld

import (
	"log"

	"gitlab.com/ParkerMc/ParkerBotGo/model"
)

// Init the module
func Init(client model.Client, handlers model.Handlers) {
	handlers.RegisterMessageUpdateHandler(
		func(message *model.Message) {
			log.Printf("%s edited a message\nBefore Edit:\n%s\nAfter Edit:\n%s", message.Author.FullUsername, message.PreEdits[len(message.PreEdits)-1].Msg, message.Msg)
			log.Print(message.Msg)
		})
}
