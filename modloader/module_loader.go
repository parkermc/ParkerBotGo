package modloader

import (
	"gitlab.com/ParkerMc/ParkerBotGo/model"
	"gitlab.com/ParkerMc/ParkerBotGo/modules/hello_world"
)

// ModuleLoader Loads the modules
type ModuleLoader struct {
}

// Load loads the modules
func (loader *ModuleLoader) Load(client model.Client, handlers model.Handlers) {
	helloworld.Init(client, handlers)
}
